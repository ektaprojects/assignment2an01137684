﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment2aN01137684.Database" %>

<asp:Content ID="CodeExample" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Database</h2>
    <div class="row">
        <h3>CREATE TABLE</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <%--<asp:DataGrid ID="teacher_sql_code" runat="server" CssClass="code"
                GridLines="None" Width="500px" CellPadding="2" >
                <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
            </asp:DataGrid>--%>

            <usercontrol:uc_code ID="Teacher_datbase_code" runat="server"
                SkinId="codedataGrid" code="Datebase" owner="Teacher">
             </usercontrol:uc_code>

        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
            <p>Code Explaination</p>
            <p>CREATE TABLE - as its name it will create table</p>
            <p>Syntax: CREATE TABLE <i>table_name</i> (</p>
            <p><i>attribute_name1 type_of_attribute</i></p>
            <p><i>attribute_name2 type_of_attribute</i></p>
            <p>)</p>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Links" ContentPlaceHolderID="Links" runat="server">
    <h2>Useful Links for Database</h2>
    <ul>
        <li><a href="https://www.w3schools.com/sql/">SQL W3C</a></li>
        <li><a href="https://www.tutorialspoint.com/sql/">Tutorials Points</a></li>
        <li><a href="https://www.tutorialspoint.com/oracle_sql/index.asp">Oracle SQL</a></li>
        <li><a href="https://www.javatpoint.com/oracle-tutorial">JavaTPoint</a></li>
        <li><a href="https://www.tutorialspoint.com/plsql/">PL/SQL</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Code" ContentPlaceHolderID="ExCode" runat="server">
    <div class="row">
        <h3>JOIN</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <h4>Example from tutorial</h4>
            <asp:DataGrid ID="tutorial_sql_code" runat="server" CssClass="code"
                GridLines="None" Width="500px" CellPadding="2" >
                <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
            </asp:DataGrid>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
            <p>Code Explaination</p>
            <p>A JOIN is used to combine two tables based on common column between them.</p>
            <p>Types of JOIN - <em>INNER,LEFT,RIGNT and FULL</em></p>
            <p>Here we used INNER JOIN</p>
            <p>It will return common records between two tables(Orders and Customers) based on CustomerID.</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="CodeExplain" ContentPlaceHolderID="logoOfContent" runat="server">
    <img class="img-thumbnail" src="images/db.png" alt="DB" />
</asp:Content>
