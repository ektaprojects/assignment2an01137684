﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2aN01137684
{
    public partial class Database : System.Web.UI.Page
    {
        //write a function here which creates a dataview
        DataView CreateTeacherCode()
        {
            DataTable databaseCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = "Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            databaseCode.Columns.Add(ln_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            databaseCode.Columns.Add(code_col);
            List<string> sql_teacher_code = new List<string>(new string[]{
                "CREATE TABLE books (",
                "~book_id NUMBER(7) PRIMARY KEY,",
                "~book_title VARCHAR2(25),",
                "~book_author VARCHAR2(20) NOT NULL,",
                "~book_type VARCHAR(15)",
                ")"
            });

            int i = 0;
            foreach (string code_line in sql_teacher_code)
            {
                coderow = databaseCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                databaseCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(databaseCode);
            return codeview;

        }
        DataView CreateTutorialCode()
        {
            DataTable databaseCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = "Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            databaseCode.Columns.Add(ln_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            databaseCode.Columns.Add(code_col);

            List<string> sql_my_code = new List<string>(new string[]{
                "SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate",
                "~FROM Orders",
                "~INNER JOIN Customers",
                "~ON Orders.CustomerID=Customers.CustomerID;"
            });

            int i = 0;
            foreach (string code_line in sql_my_code)
            {
                coderow = databaseCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                databaseCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(databaseCode);
            return codeview;

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //assign the dataview to teacher_sql_code
            //DataView dv = CreateTeacherCode();
            //teacher_sql_code.DataSource = dv;

            //teacher_sql_code.DataBind();

            DataView dv1 = CreateTeacherCode();
            tutorial_sql_code.DataSource = dv1;

            tutorial_sql_code.DataBind();
        }
    }
}