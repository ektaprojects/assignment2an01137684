﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML.aspx.cs" Inherits="Assignment2aN01137684.HTML" %>

<asp:Content ID="CodeExample" ContentPlaceHolderID="MainContent" runat="server">
    <h2>HTML</h2>
    <div class="row">
        <h3>HTML table</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <h4>Example from class</h4>
            <img class="img-thumbnail" src="images/htmlex.png" alt="DB" />
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
            <p>Code Explaination</p>
            <p>
                Here we are creating table with 3 rows and 3 columns.
                rowspan="2" means it merges the two rows.
            </p>
            <p>thead = table head</p>
            <p>tbody = table body</p>
            <p>th = table heading(Column name)</p>
            <p>tr = table row</p>
            <p>td = table data</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Links" ContentPlaceHolderID="Links" runat="server">
    <h2>Useful Links for HTML</h2>
    <ul>
        <li><a href="https://www.w3schools.com/html/">HTML W3C</a></li>
        <li><a href="https://websitesetup.org/html-tutorial-beginners/">HTML Tutorial</a></li>
        <li><a href="https://www.tutorialspoint.com/html/">Tutorials Point</a></li>
        <li><a href="https://www.w3schools.com/html/html5_intro.asp">HTML 5</a></li>
        <li><a href="https://www.quora.com/What-is-the-latest-version-of-HTML">HTML version</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Code" ContentPlaceHolderID="ExCode" runat="server">
    <div class="row">
        <h3>HTML Form</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <h4>Example from tutorial</h4>
            <img class="img-thumbnail" src="images/htmlex2.png" alt="DB" />
        </div>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <p>Code Explaination</p>
            <p>In html if we want to make a user input form then we have to use form tag with some specific properties</p>
            <p>input tag with type attribute= it will create one box according to type.</p>
            <p>If we set type=submit then it will create a submit button on form.</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="CodeExplain" ContentPlaceHolderID="logoOfContent" runat="server">
    <img class="img-thumbnail" src="images/html.png" alt="DB" />
</asp:Content>
