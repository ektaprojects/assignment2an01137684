﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="Assignment2aN01137684.JavaScript" %>

<asp:Content ID="CodeExample" ContentPlaceHolderID="MainContent" runat="server">
    <h2>JavaScript</h2>
    <div class="row">
        <h3>JavaScript Function</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <asp:DataGrid ID="teacher_js_code" runat="server" CssClass="code"
                GridLines="None" Width="500px" CellPadding="2" >
                <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
            </asp:DataGrid>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
            <p>Code Explaination</p>
            <p>This function takes the values of 5 variables and calculate average of that values and return average.</p>
            <p>.toFixed(n) function set the decimle points after the digit</p>
            <p>Here variable grade takes the returned value.</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Links" ContentPlaceHolderID="Links" runat="server">
    <h2>Useful Links for JavaScript</h2>
    <ul>
        <li><a href="https://www.w3schools.com/js/">JS W3C</a></li>
        <li><a href="https://www.tutorialspoint.com/javascript/">Tutorials Point</a></li>
        <li><a href="https://javascript.info/">JavaScript</a></li>
        <li><a href="https://developer.mozilla.org/bm/docs/Web/JavaScript">Mozilla</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Learn">Learn on mozilla</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Code" ContentPlaceHolderID="ExCode" runat="server">

    <div class="row">
        <h3>JavaScript Object</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <asp:DataGrid ID="my_js_code" runat="server" CssClass="code"
                GridLines="None" Width="500px" CellPadding="2" >
                <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
            </asp:DataGrid>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
            <p>Code Explaination</p>
            <p>
                Here we are making one object with two properties and one function
                In this code function fullname is concating firstName and lastName
                and return fullName.
            </p>
        </div>
    </div>


</asp:Content>
<asp:Content ID="courseimg" ContentPlaceHolderID="logoOfContent" runat="server">
    <img class="img-thumbnail" src="images/js.png" alt="JS" />
</asp:Content>
