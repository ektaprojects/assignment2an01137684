﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2aN01137684
{
    public partial class JavaScript : System.Web.UI.Page
    {
        DataView CreateTeacherJSCode()
        {
            DataTable jsCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = "Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            jsCode.Columns.Add(ln_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            jsCode.Columns.Add(code_col);
            List<string> js_teacher_code = new List<string>(new string[]{
                "function avg(num1,num2,num3,num4,num5)",
                "~~{,",
                "~~ar avg = 0;",
                "~~avg = (num1+num2+num3+num4+num5)/5;",
                "}",
                "var grade = avg(20,30,40,22,45);"
            });

            int i = 0;
            foreach (string code_line in js_teacher_code)
            {
                coderow = jsCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                jsCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(jsCode);
            return codeview;

        }
        DataView CreateMyJSCode()
        {
            DataTable jsCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = "Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            jsCode.Columns.Add(ln_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            jsCode.Columns.Add(code_col);
            List<string> js_my_code = new List<string>(new string[]{
                "var myObject = {",
                "firstName:Ekta,",
                "lastName: Patel,",
                "fullName: function() {",
                "return this.firstName + this.lastName;",
                "}"
            });

            int i = 0;
            foreach (string code_line in js_my_code)
            {
                coderow = jsCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                jsCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(jsCode);
            return codeview;

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            DataView dv = CreateTeacherJSCode();
            teacher_js_code.DataSource = dv;

            teacher_js_code.DataBind();

            DataView dv1 = CreateMyJSCode();
            my_js_code.DataSource = dv1;

            my_js_code.DataBind();
        }
    }
}