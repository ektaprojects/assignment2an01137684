﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2aN01137684.usercontrol
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        DataView CreateTeacherCode()
        {
            DataTable databaseCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = "Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            databaseCode.Columns.Add(ln_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            databaseCode.Columns.Add(code_col);
            List<string> sql_teacher_code = new List<string>(new string[]{
                "CREATE TABLE books (",
                "~book_id NUMBER(7) PRIMARY KEY,",
                "~book_title VARCHAR2(25),",
                "~book_author VARCHAR2(20) NOT NULL,",
                "~book_type VARCHAR(15)",
                ")"
            });

            

            int i = 0;
            foreach (string code_line in sql_teacher_code)
            {
                coderow = databaseCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                databaseCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(databaseCode);
            return codeview;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView dv = CreateTeacherCode();
            teacher_code.DataSource = dv;

            teacher_code.DataBind();
        }
    }
}